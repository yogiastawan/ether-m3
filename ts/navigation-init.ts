export function init() {
    let el = document.getElementsByClassName('nav-bar');
    for (let i = 0; i < el.length; i++) {
        doInit(el[i] as HTMLElement);
    }


}

function doInit(el: HTMLElement) {
    document.addEventListener('scroll', (event) => {
        el.dataset.onscroll = "true";
        setTimeout(() => {
            el.dataset.onscroll = "false";
        }, 300);
    });
}