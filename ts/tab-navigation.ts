import { TabNavigationRails } from "./tab-navigation-rails.js";

export class TabNavigation extends TabNavigationRails {
    constructor(id: string) {
        super(id);
    }
}