import { IconButtonToggle } from "./icon-button-toggle.js";

export class OutlinedIconButtonToggle extends IconButtonToggle {
    constructor(id: string) {
        super(id);
    }
}