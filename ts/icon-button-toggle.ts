export class IconButtonToggle {
    private id: string;
    constructor(id: string) {
        this.id = id;
    }

    onClick(callback: (event: MouseEvent, isToggle: boolean) => void) {
        const el = document.getElementById(`${this.id}-container`);
        const i = el?.children[0] as HTMLInputElement;
        if (el) el.onclick = ev => {
            callback(ev, i.checked);
            i.checked = !i.checked;
        };
    }

    setToogle(set: boolean) {
        const el = document.getElementById(`${this.id}-container`);
        (el?.children[0] as HTMLInputElement).checked = set;
    }

    getToggle(): boolean {
        const el = document.getElementById(`${this.id}-container`);
        return (el?.children[0] as HTMLInputElement).checked;
    }
}