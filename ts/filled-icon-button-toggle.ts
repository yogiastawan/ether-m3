import { IconButtonToggle } from "./icon-button-toggle.js";

export class FilledIconButtonToggle extends IconButtonToggle {
    constructor(id: string) {
        super(id);
    }
}