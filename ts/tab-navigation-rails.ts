export class TabNavigationRails {

    private id: string

    constructor(id: string) {
        this.id = id;
    }

    onClick(clickTabItem: (event: MouseEvent, id: string, link: string, position: number) => void) {
        let el = document.getElementById(this.id);
        let tab = el?.children[0].children;
        if (tab) {
            for (let i = 0; i < tab?.length; i++) {
                let el = tab[i] as HTMLAnchorElement;
                el.onclick = (ev) => {
                    (el.children[0] as HTMLInputElement).checked = true;
                    clickTabItem(ev, el.id, el.href, i);
                }

            }
        }
    }
}