import { OutlinedTextField } from "./outlined-text-field.js";

export class FilledTextField extends OutlinedTextField {

    constructor(id: string) {
        super(id);
    }

    createHTMLElement(className: string[], type: string, placeholder: string, supportTxt: string): String {
        return `<div id="${this.id}-container" class="outline-text-field ${className.join(' ')}"><input id="${this.id}" type="${type}" name="${this.id}" placeholder=""/><div></div><label>${placeholder}</label><p>${supportTxt}</p></div>`;
    }

}