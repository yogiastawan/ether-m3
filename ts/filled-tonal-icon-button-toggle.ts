import { IconButtonToggle } from "./icon-button-toggle.js";

export class FilledTonnalIconButtonToggle extends IconButtonToggle {
    constructor(id: string) {
        super(id);
    }
}