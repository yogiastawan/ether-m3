export function outlinedtextFieldInit() {
    let outlineTextFiled = document.getElementsByClassName('outlined-text-field');

    for (let i = 0; i < outlineTextFiled.length; i++) {
        let el = outlineTextFiled[i] as HTMLDivElement;
        doInit(el);
        window.onresize = (ev) => {
            doInit(el);
        };
    }

}

function doInit(elem: HTMLDivElement) {
    let w = elem.getBoundingClientRect().width;
    let svg = elem.children[2];
    if ((elem.children[0] as HTMLInputElement).value) {
        let wPlaceholder = (elem.children[1] as HTMLElement).getBoundingClientRect().width;
        (svg.children[0] as SVGPathElement).setAttribute('d', `M12,1 h-7 a4,4 0 0 0 -4,4 v46 a4,4 0 0 0 4,4 h${w - 10} a4,4 0 0 0 4,-4 v-46 a4,4 0 0 0 -4,-4 h-${w - (25 + wPlaceholder)}`);
    } else {
        (svg.children[0] as SVGPathElement).setAttribute('d', `M12,0.5 h-7.5 a4,4 0 0 0 -4,4 v47 a4,4 0 0 0 4,4 h${w - 9} a4,4 0 0 0 4,-4 v-47 a4,4 0 0 0 -4,-4 z`);
    }
    (elem.children[0] as HTMLInputElement).onfocus = (event) => {
        setTimeout(() => {
            let wPlaceholder = (elem.children[1] as HTMLElement).getBoundingClientRect().width;
            (svg.children[0] as SVGPathElement).setAttribute('d', `M12,1 h-7 a4,4 0 0 0 -4,4 v46 a4,4 0 0 0 4,4 h${w - 10} a4,4 0 0 0 4,-4 v-46 a4,4 0 0 0 -4,-4 h-${w - (25 + wPlaceholder)}`);
        }, 130)
    };

    (elem.children[0] as HTMLInputElement).addEventListener('focusout', (event) => {
        // setTimeout(() => {
        if (!(elem.children[0] as HTMLInputElement).value) {
            // let wPlaceholder = (outlineTextFiled[i].children[2] as HTMLElement).getBoundingClientRect().width;
            (svg.children[0] as SVGPathElement).setAttribute('d', `M12,0.5 h-7.5 a4,4 0 0 0 -4,4 v47 a4,4 0 0 0 4,4 h${w - 9} a4,4 0 0 0 4,-4 v-47 a4,4 0 0 0 -4,-4 z`);
        }
        // }, 130)
    });

}

export class OutlinedTextField {
    protected id: string;
    private msgDef: string | undefined;
    _isError: boolean | undefined;


    constructor(id: string) {
        this.id = id;
        let el = document.getElementById(`${id}-container`);
        this._isError = el?.classList.contains('error');
        this.msgDef = (el?.children[3] as HTMLParagraphElement).innerText;        
    }

    toHTMLElement(): HTMLElement | null {
        return document.getElementById(`${this.id}-container`);
    }

    createHTMLElement(className: string[], type: string, placeholder: string, supportTxt: string): String {
        return `<div id="${this.id}-container" class="outline-text-field ${className.join(' ')}"><input id="${this.id}" type="${type}" name="${this.id}" placeholder=""/><label>${placeholder}</label><svg><path/></svg><p>${supportTxt}</p></div>`;
    }

    setValue(value: string) {
        let el = document.getElementById(`${this.id}-container`);
        (el?.children[0] as HTMLInputElement).value = value;
    }

    getValue(): string {
        let el = document.getElementById(`${this.id}-container`);
        return (el?.children[0] as HTMLInputElement).value;
    }

    setError(msg: string | null) {
        let el = document.getElementById(`${this.id}-container`);
        el?.classList.add('error');
        if (msg) {
            (el?.children[3] as HTMLParagraphElement).innerText = msg;
        }
        this._isError = true;
    }

    removeError(msg: string | null) {
        let el = document.getElementById(`${this.id}-container`);
        el?.classList.remove('error');
        if (msg) {
            (el?.children[3] as HTMLParagraphElement).innerText = msg;
        } else {
            if (this.msgDef) {
                (el?.children[3] as HTMLParagraphElement).innerText = this.msgDef;
            }
        }
        this._isError = false;
    }

    get isError(): boolean | undefined {
        return this._isError;
    }
}