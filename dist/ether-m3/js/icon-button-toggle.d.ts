export declare class IconButtonToggle {
    private id;
    constructor(id: string);
    onClick(callback: (event: MouseEvent, isToggle: boolean) => void): void;
    setToogle(set: boolean): void;
    getToggle(): boolean;
}
