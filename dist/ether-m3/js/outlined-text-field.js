export function outlinedtextFieldInit() {
    let outlineTextFiled = document.getElementsByClassName('outlined-text-field');
    for (let i = 0; i < outlineTextFiled.length; i++) {
        let el = outlineTextFiled[i];
        doInit(el);
        window.onresize = (ev) => {
            doInit(el);
        };
    }
}
function doInit(elem) {
    let w = elem.getBoundingClientRect().width;
    let svg = elem.children[2];
    if (elem.children[0].value) {
        let wPlaceholder = elem.children[1].getBoundingClientRect().width;
        svg.children[0].setAttribute('d', `M12,1 h-7 a4,4 0 0 0 -4,4 v46 a4,4 0 0 0 4,4 h${w - 10} a4,4 0 0 0 4,-4 v-46 a4,4 0 0 0 -4,-4 h-${w - (25 + wPlaceholder)}`);
    }
    else {
        svg.children[0].setAttribute('d', `M12,0.5 h-7.5 a4,4 0 0 0 -4,4 v47 a4,4 0 0 0 4,4 h${w - 9} a4,4 0 0 0 4,-4 v-47 a4,4 0 0 0 -4,-4 z`);
    }
    elem.children[0].onfocus = (event) => {
        setTimeout(() => {
            let wPlaceholder = elem.children[1].getBoundingClientRect().width;
            svg.children[0].setAttribute('d', `M12,1 h-7 a4,4 0 0 0 -4,4 v46 a4,4 0 0 0 4,4 h${w - 10} a4,4 0 0 0 4,-4 v-46 a4,4 0 0 0 -4,-4 h-${w - (25 + wPlaceholder)}`);
        }, 130);
    };
    elem.children[0].addEventListener('focusout', (event) => {
        if (!elem.children[0].value) {
            svg.children[0].setAttribute('d', `M12,0.5 h-7.5 a4,4 0 0 0 -4,4 v47 a4,4 0 0 0 4,4 h${w - 9} a4,4 0 0 0 4,-4 v-47 a4,4 0 0 0 -4,-4 z`);
        }
    });
}
export class OutlinedTextField {
    constructor(id) {
        this.id = id;
        let el = document.getElementById(`${id}-container`);
        this._isError = el === null || el === void 0 ? void 0 : el.classList.contains('error');
        this.msgDef = (el === null || el === void 0 ? void 0 : el.children[3]).innerText;
    }
    toHTMLElement() {
        return document.getElementById(`${this.id}-container`);
    }
    createHTMLElement(className, type, placeholder, supportTxt) {
        return `<div id="${this.id}-container" class="outline-text-field ${className.join(' ')}"><input id="${this.id}" type="${type}" name="${this.id}" placeholder=""/><label>${placeholder}</label><svg><path/></svg><p>${supportTxt}</p></div>`;
    }
    setValue(value) {
        let el = document.getElementById(`${this.id}-container`);
        (el === null || el === void 0 ? void 0 : el.children[0]).value = value;
    }
    getValue() {
        let el = document.getElementById(`${this.id}-container`);
        return (el === null || el === void 0 ? void 0 : el.children[0]).value;
    }
    setError(msg) {
        let el = document.getElementById(`${this.id}-container`);
        el === null || el === void 0 ? void 0 : el.classList.add('error');
        if (msg) {
            (el === null || el === void 0 ? void 0 : el.children[3]).innerText = msg;
        }
        this._isError = true;
    }
    removeError(msg) {
        let el = document.getElementById(`${this.id}-container`);
        el === null || el === void 0 ? void 0 : el.classList.remove('error');
        if (msg) {
            (el === null || el === void 0 ? void 0 : el.children[3]).innerText = msg;
        }
        else {
            if (this.msgDef) {
                (el === null || el === void 0 ? void 0 : el.children[3]).innerText = this.msgDef;
            }
        }
        this._isError = false;
    }
    get isError() {
        return this._isError;
    }
}
