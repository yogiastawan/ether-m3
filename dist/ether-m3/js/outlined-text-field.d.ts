export declare function outlinedtextFieldInit(): void;
export declare class OutlinedTextField {
    protected id: string;
    private msgDef;
    _isError: boolean | undefined;
    constructor(id: string);
    toHTMLElement(): HTMLElement | null;
    createHTMLElement(className: string[], type: string, placeholder: string, supportTxt: string): String;
    setValue(value: string): void;
    getValue(): string;
    setError(msg: string | null): void;
    removeError(msg: string | null): void;
    get isError(): boolean | undefined;
}
