export class TabNavigationRails {
    constructor(id) {
        this.id = id;
    }
    onClick(clickTabItem) {
        let el = document.getElementById(this.id);
        let tab = el === null || el === void 0 ? void 0 : el.children[0].children;
        if (tab) {
            for (let i = 0; i < (tab === null || tab === void 0 ? void 0 : tab.length); i++) {
                let el = tab[i];
                el.onclick = (ev) => {
                    el.children[0].checked = true;
                    clickTabItem(ev, el.id, el.href, i);
                };
            }
        }
    }
}
