import { OutlinedTextField } from "./outlined-text-field.js";
export declare class FilledTextField extends OutlinedTextField {
    constructor(id: string);
    createHTMLElement(className: string[], type: string, placeholder: string, supportTxt: string): String;
}
