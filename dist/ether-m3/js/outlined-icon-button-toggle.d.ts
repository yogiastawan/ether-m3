import { IconButtonToggle } from "./icon-button-toggle.js";
export declare class OutlinedIconButtonToggle extends IconButtonToggle {
    constructor(id: string);
}
