import { IconButtonToggle } from "./icon-button-toggle.js";
export declare class FilledIconButtonToggle extends IconButtonToggle {
    constructor(id: string);
}
