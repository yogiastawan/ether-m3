export declare class TabNavigationRails {
    private id;
    constructor(id: string);
    onClick(clickTabItem: (event: MouseEvent, id: string, link: string, position: number) => void): void;
}
