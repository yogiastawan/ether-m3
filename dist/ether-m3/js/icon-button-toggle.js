export class IconButtonToggle {
    constructor(id) {
        this.id = id;
    }
    onClick(callback) {
        const el = document.getElementById(`${this.id}-container`);
        const i = el === null || el === void 0 ? void 0 : el.children[0];
        if (el)
            el.onclick = ev => {
                callback(ev, i.checked);
                i.checked = !i.checked;
            };
    }
    setToogle(set) {
        const el = document.getElementById(`${this.id}-container`);
        (el === null || el === void 0 ? void 0 : el.children[0]).checked = set;
    }
    getToggle() {
        const el = document.getElementById(`${this.id}-container`);
        return (el === null || el === void 0 ? void 0 : el.children[0]).checked;
    }
}
