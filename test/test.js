"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const sass_1 = __importDefault(require("sass"));
const fs_1 = __importDefault(require("fs"));
const app = (0, express_1.default)();
app.set('view engine', "ejs");
app.set('view cache', true);
app.set('views', ['test', 'dist/ether-m3/template']);
app.use(express_1.default.static('dist'));
app.use(express_1.default.static('test/style'));
app.use(express_1.default.static('test/js'));
app.use(express_1.default.static('test/img'));
app.use('/build', (req, res, next) => {
    const a = sass_1.default.compileAsync('test/style/style.scss');
    const b = sass_1.default.compileAsync('scss/ether-m3.scss');
    const c = sass_1.default.compileAsync('scss/theme/theme-default.scss');
    Promise.all([a, b, c]).then((result) => {
        fs_1.default.writeFileSync('test/style/style.css', result[0].css);
        fs_1.default.writeFileSync('dist/ether-m3/css/ether-m3.css', result[1].css);
        fs_1.default.mkdirSync('dist/ether-m3/css/theme', { recursive: true });
        fs_1.default.writeFileSync('dist/ether-m3/css/theme/theme-default.css', result[2].css);
        next();
    }).catch(err => {
        res.json({
            success: false,
            msg: JSON.parse(err),
        });
    });
});
app.get('/', (req, res) => {
    res.render('index', { version: `1.0.0` });
});
app.post('/build', (req, res) => {
    res.json({
        success: true
    });
});
app.listen(3000, () => {
    console.log("server running on port 3000. http://localhost:3000");
});
