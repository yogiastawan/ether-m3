import { TabNavigationRails } from "/ether-m3/js/tab-navigation-rails.js";
import { TabNavigation } from "/ether-m3/js/tab-navigation.js";
import { outlinedtextFieldInit } from "/ether-m3/js/outlined-text-field.js";
import { IconButtonToggle } from "/ether-m3/js/icon-button-toggle.js";
import { FilledIconButtonToggle } from "/ether-m3/js/filled-icon-button-toggle.js";
import { FilledTonnalIconButtonToggle } from "/ether-m3/js/filled-tonal-icon-button-toggle.js";
import { OutlinedIconButtonToggle } from "/ether-m3/js/outlined-icon-button-toggle.js";


outlinedtextFieldInit();

const tab = new TabNavigationRails('tabnav1');
tab.onClick((ev, id, link, i) => {
    ev.preventDefault();
    console.log("tabnav1", id, link, i);
});

const tab2 = new TabNavigation('tabnav2');
tab2.onClick((ev, id, link, i) => {
    ev.preventDefault();
    console.log("tabnav2", id, link, i);
});

const tab3 = new TabNavigation('tabnav3');
tab3.onClick((ev, id, link, i) => {
    ev.preventDefault();
    console.log("tabnav3", id, link, i);
});

const b = document.getElementById('btn-build');

if (b) b.onclick = (event) => {
    console.log('build');
    fetch('/build', {
        method: 'post',
        body: null,
        headers: {
            'Accept': 'application/json',
            'content-type': 'application/json'
        }
    }).then(res => {
        const m = document.getElementById('err');
        res.json().then(result => {
            if (result.success) {
                const a = document.getElementsByTagName('link');
                for (let i = 0; i < a.length; i++) {
                    a[i].href = `${a[i].href}?v=${Math.random()}`
                }
                if (m) m.innerText = "Success";
            } else {
                if (m) m.innerText = `Err: ${result.msg}`;
            }
        }).catch(err => {
            if (m) m.innerText = `Err: ${err}`;
        });

    }).catch(err => {
        const m = document.getElementById('err');
        if (m) m.innerText = `Err: ${err}`;
    });
};

//add icon button toggle

const icBtn1 = new IconButtonToggle('btn1');
icBtn1.onClick((ev, toggle) => {
    ev.preventDefault();
    console.log("icon button 1 is", toggle);
});

const icBtn2 = new FilledTonnalIconButtonToggle('btn2');
icBtn2.onClick((ev, toggle) => {
    ev.preventDefault();
    console.log("icon button 2 is", toggle);
});

const icBtn3 = new FilledIconButtonToggle('btn3');
icBtn3.onClick((ev, toggle) => {
    ev.preventDefault();
    console.log("icon button 3 is", toggle);
});

const icBtn4 = new OutlinedIconButtonToggle('btn4');
icBtn4.onClick((ev, toggle) => {
    ev.preventDefault();
    console.log("icon button 4 is", toggle);
});