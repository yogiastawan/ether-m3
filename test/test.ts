import express from 'express';
import sass from 'sass';
import fs from 'fs';
// import LRU from 'lru-cache';
// import ejs from 'ejs';

const app = express();

//not needed
// const opt = {
//     max: 500,
//     ttl: 60000,
//     alloStale: true,
//     // for use with tracking overall storage size
//     maxSize: 5000, 
//     sizeCalculation: (value:any, key:any) => {
//         return 1
//     }
// }
// ejs.cache = new LRU(opt);

app.set('view engine', "ejs");
app.set('view cache', true);
app.set('views', ['test', 'dist/ether-m3/template']);

// console.log(app.get('view cache'));


app.use(express.static('dist'));
app.use(express.static('test/style'));
app.use(express.static('test/js'));
app.use(express.static('test/img'));



app.use('/build', (req, res, next) => {
    //compile scss
    const a = sass.compileAsync('test/style/style.scss');
    const b = sass.compileAsync('scss/ether-m3.scss');
    const c = sass.compileAsync('scss/theme/theme-default.scss');

    Promise.all([a, b, c]).then((result) => {
        fs.writeFileSync('test/style/style.css', result[0].css);
        fs.writeFileSync('dist/ether-m3/css/ether-m3.css', result[1].css);
        fs.mkdirSync('dist/ether-m3/css/theme', { recursive: true });
        fs.writeFileSync('dist/ether-m3/css/theme/theme-default.css', result[2].css);
        next();
    }).catch(err => {
        res.json({
            success: false,
            msg: JSON.parse(err),
        });
    })

});

app.get('/', (req, res) => {
    //send respon
    res.render('index', { version: `1.0.0` });
});

app.post('/build', (req, res) => {
    res.json({
        success: true
    })
})


app.listen(3000, () => {
    console.log("server running on port 3000. http://localhost:3000");
});